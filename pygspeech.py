"""
Simple way to access google api for speech recognition with python

Copyright (c) 2013, Carlos Ganoza Plasencia
url: http://about.me/drneox
"""
from pyaudio import PyAudio, paInt16
from wave import open as open_audio
from urllib2 import Request, urlopen
from os import system
from json import loads
import time


class Pygspeech:
    def __init__(self, file="audio"):
        self.format = paInt16
        self.rate = 8000
        self.channel = 1
        self.chunk = 1024
        self.file = file

    def convert(self):
        system("sox %s -t wav -r 8000 -t flac %s.flac" % (self.file, self.file))

    def record(self, time):
        audio = PyAudio()
        stream = audio.open(format=self.format, channels=self.channel,
                            rate=self.rate, input=True,
                            frames_per_buffer=self.chunk)
        print "REC: "
        frames = []
        for i in range(0, self.rate / self.chunk * time):
            data = stream.read(self.chunk)
            frames.append(data)
        stream.stop_stream()
        stream.close()
        audio.terminate()
        write_frames = open_audio(self.file, 'wb')
        write_frames.setnchannels(self.channel)
        write_frames.setsampwidth(audio.get_sample_size(self.format))
        write_frames.setframerate(self.rate)
        write_frames.writeframes(''.join(frames))
        write_frames.close()
        self.convert()

    def speech_to_text(self, language):
        url = "http://www.google.com/speech-api/v1/recognize?lang=%s" % language
        file_upload = "%s.flac" % self.file
        audio = open(file_upload, "rb").read()
        # Headers. A common Chromium (Linux) User-Agent
        header = {"User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.63 Safari/535.7", 
           'Content-type': 'audio/x-flac; rate=16000'} 
        data = Request(url, audio, header)
        post = urlopen(data)
        response = post.read()
        phrase = loads(response)['hypotheses'][0]['utterance']
        return phrase, response

    def text_to_speech(self, text='hello', language='en', fname='result.wav', player=None):
        """ Sends text to Google's text to speech service
        and returns created speech (wav file). """

        limit = min(100, len(text))#100 characters is the current limit.
        text = text[0:limit]
        print "Text to speech:", text
        url = "http://translate.google.com/translate_tts"
        values = urllib.urlencode({"q": text, "textlen": len(text), "tl": lang})
        hrs = {"User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.63 Safari/535.7"}
        #TODO catch exceptions
        req = urllib2.Request(url, data=values, headers=hrs)
        p = urllib2.urlopen(req)
        f = open(fname, 'wb')
        f.write(p.read())
        f.close()
        print "Speech saved to:", fname
        if player is not None:
            play_wav(fname, player)


    def play_wav(filep, player='mplayer'):
        ''' Plays filep using player '''
        print "Playing %s file using %s" % (filep, player)
        try:
            os.system(player + " " + filep)
        except:
            print "Couldn't use %s to play file" % (player)
